package pico.jennfier.facci.practica4y5;

import android.app.Dialog;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import static pico.jennfier.facci.practica4y5.R.menu.main;

public class ActividadPrincipal extends AppCompatActivity implements View.OnClickListener, FragUno.OnFragmentInteractionListener, FragDos.OnFragmentInteractionListener{
    Button botonFragUno, botonFragDos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_principal);
        botonFragUno = (Button) findViewById(R.id.btnFrgUno);
        botonFragDos = (Button) findViewById(R.id.btnFrgDos);
        botonFragUno.setOnClickListener(this);
        botonFragDos.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnFrgUno:
                FragUno fragmentoUno = new FragUno();
                FragmentTransaction transactionUno = getSupportFragmentManager().beginTransaction();
                transactionUno.replace(R.id.contenedor,fragmentoUno);
                transactionUno.commit();
                break;
            case R.id.btnFrgDos:
                FragDos fragmentoDos = new FragDos();
                FragmentTransaction transactionDos = getSupportFragmentManager().beginTransaction();
                transactionDos.replace(R.id.contenedor,fragmentoDos);
                break;
        }

    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.opcionLogin:
                Dialog dialogoLogin = new Dialog(ActividadPrincipal.this);
                dialogoLogin.setContentView(R.layout.dlg_log);

                Button botonAutenticar= (Button)dialogoLogin.findViewById(R.id.btnAutenticar);
                final EditText cajaUsuario = (EditText) dialogoLogin.findViewById(R.id.txtUser);
                final EditText cajaClave =(EditText) dialogoLogin.findViewById(R.id.txtPassword);

                botonAutenticar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(ActividadPrincipal.this, cajaUsuario.getText().toString() + "" + cajaClave.getText().toString(),Toast.LENGTH_LONG).show();
                    }
                });

                dialogoLogin.show();
                break;
            case R.id.opcionRegistrar:
                break;

        }
        return true;

    }



}
